# Scripts
git clone https://github.com/GustavoMends/scripts && cd scripts && bash setup/android_build_env.sh && cd ..

# Repo
curl https://storage.googleapis.com/git-repo-downloads/repo > repo && chmod a+x repo && sudo install repo /usr/local/bin && rm repo 

# GitUser
git config --global user.name "Gustavo Mendes"  && git config --global user.email "gusttavo.me@outlook.com"  

# Add the Gerrit Change-id hook
mkdir -p ~/.git/hooks && git config --global core.hooksPath ~/.git/hooks && curl -Lo ~/.git/hooks/commit-msg https://review.lineageos.org/tools/hooks/commit-msg && chmod u+x ~/.git/hooks/commit-msg
